$(function() {
    $('.modal').modal();

    $('.delete-employee').click(function() {
		var id = $(this).data('id');
		$('#modal-agree-button').attr('href', `./delete-employee.php?id=${id}`);
	})
});

