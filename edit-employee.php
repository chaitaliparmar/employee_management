<?php 
require_once "includes/functions.php";
$page_load_flag = false;
if(isset($_GET['id']) || isset($_POST['action']))
{
	//$id = ($_REQUEST['id']);
	//die(var_dump($_REQUEST));
	$page_load_flag = false;
}
	//die('hello World');
if(isset($_POST['name'])){

	//die('hello World');
	$id = ($_POST['id']);
	$name =($_POST['name']);
	$address1 =($_POST['address1']);
	$address2 =($_POST['address2']);
	$location =($_POST['location']);
	$zip =($_POST['zip']);
	$postal =($_POST['postal']);
	$taluka =($_POST['taluka']);
	$suburb =($_POST['suburb']);
	$east =($_POST['east']);
	$city =($_POST['city']);
	$district =($_POST['district']);
	$state =($_POST['state']);
	$country =($_POST['country']);


	$row = db_select("SELECT * FROM employee WHERE id = $id");
	/*if(!$row)
	{
		(db_error());
	}
	else
	{}*/
	$query = "UPDATE `employee` SET `name` = '$name',`address1`='$address1',`address2`='$address2',`location`='$location',`zip`='$zip',`postal`='$postal',`taluka`='$taluka',`suburb`='$suburb',`east`='$east',`city`='$city',`district`='$district',`state`='$state',`country`='$country' WHERE id = $id";
	$result = db_query($query);


	if($result)
	{
		header("Location: index.php?q=success&op=update");

	}
	else{
		$error_flag = true;
	}
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Employee Management</title>
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
 <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    <h1 class="title">Edit Employee Details</h1>

<?php
	$id = $_GET['id'];
    $row = db_select("SELECT * FROM employee WHERE id = $id");
    //die(var_dump($row));

?>

    <form class="form" method="POST" action="<?= $_SERVER['PHP_SELF'];?>">



  <div class="form-row">
    <div class="form-group col-md-4">

      <label>Employee Name</label>
      <input type="text" class="form-control" id="inputEmail4" placeholder="Employee Name" name="name" value="<?=$row[0]['name'];?>">
    </div>
      <h3 class="address-title">Address Details</h3>

    <div class="form-group col-md-12 address_detail_1">
        <div class="col-md-4 inner_input">
            <label for="inputPassword4">Address 1*</label>
         <input type="text" class="form-control"  placeholder="Address 1" name="address1" value="<?=$row[0]['address1'];?>">
        </div>
         <div class="col-md-4 inner_input">
            <label for="inputPassword4">Address 2</label>
         <input type="text" class="form-control" id="inputPassword4" placeholder="Address 2" name="address2" value="<?=$row[0]['address2'];?>">
        </div>
         <div class="col-md-4 inner_input">
            <label for="inputPassword4">Location</label>
         <input type="text" class="form-control" id="inputPassword4" placeholder="Location" name="location" value="<?=$row[0]['location'];?>">
        </div>
    </div>

  </div>
   <div class="form-group col-md-12 address_detail_1">
        <div class="col-md-4 inner_input">
            <label for="inputPassword4">Zip/Postal Code</label>
         <input type="text" class="form-control" id="inputPassword4" placeholder="000000" name="zip" value="<?=$row[0]['zip'];?>">
        </div>

         <div class="col-md-4 inner_input">
            <label for="inputPassword4">Postal Area</label>
         <input type="text" class="form-control" id="inputPassword4" placeholder="Postal Area" name="postal" value="<?=$row[0]['postal'];?>">
        </div>
         <div class="col-md-4 inner_input">
            <label for="inputPassword4">Taluka</label>
         <input type="text" class="form-control" id="inputPassword4" placeholder="Taluka" name="taluka" value="<?=$row[0]['taluka'];?>">
        </div>
    </div>
     <div class="form-group col-md-12 address_detail_1">
        <div class="col-md-4 inner_input">
            <label for="inputPassword4">Suburb</label>
         <input type="text" class="form-control" id="inputPassword4" placeholder="Suburb" name="suburb" value="<?=$row[0]['suburb'];?>">
        </div>
         <div class="col-md-4 inner_input">
            <label for="inputPassword4">East/West</label>
         <input type="text" class="form-control" id="inputPassword4" placeholder="East/West" name="east" value="<?=$row[0]['east'];?>">
        </div>
         <div class="col-md-4 inner_input">
            <label for="inputPassword4">City*</label>
         <input type="text" class="form-control" id="inputPassword4" placeholder="City" name="city" value="<?=$row[0]['city'];?>">
        </div>
    </div>
   <div class="form-group col-md-12 address_detail_1">
        <div class="col-md-4 inner_input">
            <label for="inputPassword4">district</label>
         <input type="text" class="form-control" id="inputPassword4" placeholder="district" name="district" value="<?=$row[0]['district'];?>">
        </div>
        <div class="col-md-4 inner_input">
          <label for="inputPassword4">State</label>
          <select class="form-control" name="state" value="<?=$row[0]['state'];?>">
          <option>maharastra</option>
          <option>pune</option>
          <option>punjab</option>
          </select>
        </div>
        <div class="col-md-4 inner_input">
          <label for="inputPassword4">Country</label>
          <select class="form-control" name="country" value="<?=$row[0]['country'];?>">
          <option>india</option>
          <option>america</option>
          <option>china</option>
          </select>
        </div>
        </div>
    </div>
  <h3 class="contact-title">Contact Details</h3>
  <div class="form-group col-md-12 next-section">
    <div class="contact">
      <div class="content-wrapper ">
          <p class="text">Mobile Number</p>
          <i id ="add" class="fas fa-plus-square icon"></i>
      </div>
       <div class=" inner_input">
        <div class="input-wrap">
          <label for="inputPassword4 " class="mobile-label">Mobile Number</label>
          <div>
             <div class="custom-control custom-radio">
         <input type="radio" name="radioDisabled" id="customRadioDisabled2" class="custom-control-input" >
        <label class="custom-control-label" for="customRadioDisabled2">Primary</label>
        </div>
          </div>
        </div>
            
           

         <input type="password" class="form-control" id="inputPassword4" placeholder="0000000">
        </div>
    </div>
      <div class="contact">
      <div class="content-wrapper">
          <p class="text">Whatsapp Number</p>
          <i id ="add" class="fas fa-plus-square icon"></i>
      </div>
       <div class=" inner_input">
        <div class="input-wrap">
          <label for="inputPassword4 " class="mobile-label">Whatsapp Number</label>
          <div>
             <div class="custom-control custom-radio">
         <input type="radio" name="radioDisabled" id="customRadioDisabled2" class="custom-control-input" >
        <label class="custom-control-label" for="customRadioDisabled2">Primary</label>
        </div>
          </div>
        </div>
            
           

         <input type="text" class="form-control" id="inputPassword4" placeholder="00000000">
        </div>
    </div>
      <div id ="textContent" class="contact">
      <div class="content-wrapper">
          <p class="text" >Email Address</p>
          <i id ="add" class="fas fa-plus-square icon"></i>
      </div>
       <div class=" inner_input">
        <div class="input-wrap">
          <label for="inputPassword4 " class="mobile-label">Email Address</label>
          <div>
             <div class="custom-control custom-radio">
         <input type="radio" name="radioDisabled" id="customRadioDisabled2" class="custom-control-input" 
         >
        <label class="custom-control-label" for="customRadioDisabled2">Primary</label>
        </div>
          </div>
        </div>
            
           

         <input type="text" class="form-control" id="inputPassword4" placeholder="abc@gmail.com">
        </div>
    </div>
  </div>

<button type="submit" class="btn btn-primary" name="action" value="Submit">Save</button>
<input type ="text" class="" name="id" value="<?=$id;?>" hidden>
</form>

 
  
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

<script src="js/jquery.min.js" type="text/javascript"></script>
   
    <script type="text/javascript" src="js/materialize.min.js"></script>
  
    <script src="js/pages/home.js"></script>
  
    <script src="js/custom.js" type="text/javascript"></script>
</body>
</html>