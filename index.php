<?php
require_once "includes/functions.php";
// die(var_dump($_POST));

if(isset($_POST['action']))
{
  $name = $_POST['name'];
  $address1 = $_POST['address1'];
  $address2 = $_POST['address2'];
  $location = $_POST['location'];
  $zip = $_POST['zip'];
  $postal = $_POST['postal'];
  $taluka = $_POST['taluka'];
  $suburb = $_POST['suburb'];
  $east = $_POST['east'];
  $city = $_POST['city'];
  $district = $_POST['district'];
  $state = $_POST['state'];
  $country = $_POST['country'];

   $query = "INSERT INTO `employee`(`name`,`address1`, `address2`, `location`, `zip`, `postal`, `taluka`, `suburb`,`east`,`city`,`district`,`state`,`country`) VALUES ('{$name}','{$address1}','{$address2}','{$location}','{$zip}','{$postal}','{$taluka}','{$suburb}','{$east}','{$city}','{$district}','{$state}','{$country}')";
        $result = db_query($query);
      //die(var_dump($_POST));

    if(!$result)
    {
        echo db_error($result);
    }
    else
    {
        echo "Records added successfully.";
    }

}

?>



<!DOCTYPE html>
<html>
<head>
    <title>Employee Management</title>
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
 <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    <h1 class="title">Employee Details</h1>
    <form class="form" method="POST" action="<?= $_SERVER['PHP_SELF'];?>">
  <div class="form-row">
    <div class="form-group col-md-4">
      <label>Employee Name</label>
      <input type="text" class="form-control" id="inputEmail4" placeholder="Employee Name" name="name">
    </div>
      <h3 class="address-title">Address Details</h3>

    <div class="form-group col-md-12 address_detail_1">
        <div class="col-md-4 inner_input">
            <label for="inputPassword4">Address 1*</label>
         <input type="text" class="form-control"  placeholder="Address 1" name="address1">
        </div>
         <div class="col-md-4 inner_input">
            <label for="inputPassword4">Address 2</label>
         <input type="text" class="form-control" id="inputPassword4" placeholder="Address 2" name="address2">
        </div>
         <div class="col-md-4 inner_input">
            <label for="inputPassword4">Location</label>
         <input type="text" class="form-control" id="inputPassword4" placeholder="Location" name="location">
        </div>
    </div>
  </div>
   <div class="form-group col-md-12 address_detail_1">
        <div class="col-md-4 inner_input">
            <label for="inputPassword4">Zip/Postal Code</label>
         <input type="text" class="form-control" id="inputPassword4" placeholder="000000" name="zip">
        </div>

         <div class="col-md-4 inner_input">
            <label for="inputPassword4">Postal Area</label>
         <input type="text" class="form-control" id="inputPassword4" placeholder="Postal Area" name="postal">
        </div>
         <div class="col-md-4 inner_input">
            <label for="inputPassword4">Taluka</label>
         <input type="text" class="form-control" id="inputPassword4" placeholder="Taluka" name="taluka">
        </div>
    </div>
     <div class="form-group col-md-12 address_detail_1">
        <div class="col-md-4 inner_input">
            <label for="inputPassword4">Suburb</label>
         <input type="text" class="form-control" id="inputPassword4" placeholder="Suburb" name="suburb">
        </div>
         <div class="col-md-4 inner_input">
            <label for="inputPassword4">East/West</label>
         <input type="text" class="form-control" id="inputPassword4" placeholder="East/West" name="east">
        </div>
         <div class="col-md-4 inner_input">
            <label for="inputPassword4">City*</label>
         <input type="text" class="form-control" id="inputPassword4" placeholder="City" name="city">
        </div>
    </div>
   <div class="form-group col-md-12 address_detail_1">
        <div class="col-md-4 inner_input">
            <label for="inputPassword4">district</label>
         <input type="text" class="form-control" id="inputPassword4" placeholder="district" name="district">
        </div>
        <div class="col-md-4 inner_input">
          <label for="inputPassword4">State</label>
          <select class="form-control" name="state">
          <option>maharastra</option>
          <option>pune</option>
          <option>punjab</option>
          </select>
        </div>
        <div class="col-md-4 inner_input">
          <label for="inputPassword4">Country</label>
          <select class="form-control" name="country">
          <option>india</option>
          <option>america</option>
          <option>china</option>
          </select>
        </div>
        </div>
    </div>
  <h3 class="contact-title">Contact Details</h3>
  <div class="form-group col-md-12 next-section">
    <div class="contact">
      <div class="content-wrapper ">
          <p class="text">Mobile Number</p>
          <i id ="add" class="fas fa-plus-square icon"></i>
      </div>
       <div class=" inner_input">
        <div class="input-wrap">
          <label for="inputPassword4 " class="mobile-label">Mobile Number</label>
          <div>
             <div class="custom-control custom-radio">
         <input type="radio" name="radioDisabled" id="customRadioDisabled2" class="custom-control-input" >
        <label class="custom-control-label" for="customRadioDisabled2">Primary</label>
        </div>
          </div>
        </div>
            
           

         <input type="password" class="form-control" id="inputPassword4" placeholder="0000000">
        </div>
    </div>
      <div class="contact">
      <div class="content-wrapper">
          <p class="text">Whatsapp Number</p>
          <i id ="add" class="fas fa-plus-square icon"></i>
      </div>
       <div class=" inner_input">
        <div class="input-wrap">
          <label for="inputPassword4 " class="mobile-label">Whatsapp Number</label>
          <div>
             <div class="custom-control custom-radio">
         <input type="radio" name="radioDisabled" id="customRadioDisabled2" class="custom-control-input" >
        <label class="custom-control-label" for="customRadioDisabled2">Primary</label>
        </div>
          </div>
        </div>
            
           

         <input type="text" class="form-control" id="inputPassword4" placeholder="00000000">
        </div>
    </div>
      <div id ="textContent" class="contact">
      <div class="content-wrapper">
          <p class="text" >Email Address</p>
          <i id ="add" class="fas fa-plus-square icon"></i>
      </div>
       <div class=" inner_input">
        <div class="input-wrap">
          <label for="inputPassword4 " class="mobile-label">Email Address</label>
          <div>
             <div class="custom-control custom-radio">
         <input type="radio" name="radioDisabled" id="customRadioDisabled2" class="custom-control-input" 
         >
        <label class="custom-control-label" for="customRadioDisabled2">Primary</label>
        </div>
          </div>
        </div>
            
           

         <input type="text" class="form-control" id="inputPassword4" placeholder="abc@gmail.com">
        </div>
    </div>
  </div>
<button type="submit" class="btn btn-primary" name="action" value="Submit">Save</button>
</form>

 
  <div>
    <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Name</th>
      <th scope="col">Address 1</th>
      <th scope="col">Address 2</th>
      <th scope="col">Location</th>
      <th scope="col">Zip code</th>
      <th scope="col">Postal</th>
      <th scope="col">Taluka</th>
      <th scope="col">Suburb</th>
      <th scope="col">East/West</th>
      <th scope="col">City</th>
      <th scope="col">district</th>
      <th scope="col">State</th>
      <th scope="col">Country</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
<?php 
            $query = "SELECT * FROM employee";
            $result = db_select($query);
            if(!$result){
              dd(db_error());
            }
            foreach ($result as $row):  
?>
    <tr>
      <th scope="row"></th>
      <td><?= $row['id'];?></td>
      <td><?= $row['name'];?></td>
      <td><?= $row['address1'];?></td>
      <td><?= $row['address2'];?></td>
      <td><?= $row['location'];?></td>
      <td><?= $row['zip'];?></td>
      <td><?= $row['postal'];?></td>
      <td><?= $row['taluka'];?></td>
      <td><?= $row['suburb'];?></td>
      <td><?= $row['east'];?></td>
      <td><?= $row['city'];?></td>
      <td><?= $row['district'];?></td>
      <td><?= $row['state'];?></td>
      <td><?= $row['country'];?></td>
      <td><a href="edit-employee.php?id=<?= $row['id'];?>"><i class="far fa-edit edit-icon"></i></a>
        <a data-id="<?=$row['id'];?>" class="modal-trigger delete-employee" href="#deleteModal"><i class="fas fa-trash material-icons"></a></i></td>
    </tr>
<?php
      endforeach;
?>
  </tbody>
</table>
  </div>

  <div id="deleteModal" class="modal">
    <div class="modal-content">
      <h4>Delete Contact?</h4>
      <p>Are you sure you want to delete the record?</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close btn blue-grey lighten-2 waves-effect">Cancel</a>
      <a href="#" id="modal-agree-button" class="modal-close btn waves-effect red lighten-2">Agree</a>
    </div>
  </div>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9P
lx0x4" crossorigin="anonymous"></script>

<script src="js/jquery.min.js" type="text/javascript"></script>
   
    <script type="text/javascript" src="js/materialize.min.js"></script>
  
    <script src="js/pages/home.js"></script>
  
    <script src="js/custom.js" type="text/javascript"></script>
</body>
</html>